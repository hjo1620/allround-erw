SETLOCAL

::
SET _machine=%COMPUTERNAME%
SET _uzer=%USERNAME%
SET _what=/MIR
SET _logdir=C:\tmp
SET _options=/R:0 /W:0 /LOG:%_logdir%\bup-%_machine%-%date%.log /NFL /NDL

:: Mirror source to destination. Delete changes on target.
:: 1
echo "1/3"
SET _source=C:\Users\%_uzer%
SET _dest=\\192.168.86.42\archive\bup\%_machine%\%_uzer%
CALL :touch %_source%
ROBOCOPY %_source% %_dest% %_what% %_options%

:: 2
echo "2/3"
SET _source=D:\app
SET _dest=\\192.168.86.42\archive\bup\%_machine%\app
CALL :touch %_source%
ROBOCOPY %_source% %_dest% %_what% %_options%

:: 3
echo "3/3"
SET _source=D:\gdrive
SET _dest=\\192.168.86.42\archive\bup\%_machine%\gdrive
CALL :touch %_source%
ROBOCOPY %_source% %_dest% %_what% %_options%

ENDLOCAL

:: Create file, if not exists, and update timestamp
:touch
SETLOCAL
cd . >> %1\bup.timestamp
copy /b %1\bup.timestamp +,,
ENDLOCAL
